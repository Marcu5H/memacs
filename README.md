# Memacs

Memacs is a tiny GNU Emacs configuration.

## Installing

Clone this git repository into `~/.emacs.d`:
```console
$ git clone https://gitlab.com/Marcu5H/memacs.git ~/.emacs.d
```

If icons are not showing correctly, install the required fonts with `M-x all-the-icons-install-fonts`.

## Configuration

Basic configuration is done by editing `configuration.el`.
If any packages/features/languages are missing, open an
[issue](https://gitlab.com/Marcu5H/memacs/-/issues/new).
